import * as passport from 'passport';
import * as jwt from 'jsonwebtoken';
import {/* hash, */compare} from "bcryptjs"
import { Application, Request/* , Response, RequestHandler, response */ } from 'express';
import { Strategy, StrategyOptions, ExtractJwt, VerifiedCallback } from 'passport-jwt';
import { User ,getUserById } from "./entity/User";
import { verify } from 'jsonwebtoken';
import { createAccessToken, createRefreshToken } from './auth';
import { sendRefreshToken } from './sendRefreshToken';


interface JwtPayload {
    userId: string;
    tokenVersion: number;
}

const JWT_SECRET_KEY = process.env.REFRESH_TOKEN_SECRET;

/** Options for JWT
 * in this exemple we use fromAuthHeader, so the client need to
 * provide an "Authorization" request header token
 */

const jwtOptions: StrategyOptions = {
    jwtFromRequest:ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: process.env.ACCESS_TOKEN_SECRET,
    passReqToCallback: true,
};

const jwtStategy = new Strategy(jwtOptions,(req: Request, jwtPayload: JwtPayload, done: VerifiedCallback) => {
        
        // In the login we encrypt the payload

        if (!jwtPayload.userId) {
            throw new Error('No userId in the JWT session token');
            
        }
        // console.log(jwtPayload.userId)
        getUserById(jwtPayload.userId)
            .then((user) => {
                // console.log(user)
                if (user) {
                    return done(null, user);                   
                } else {
                    return done(null, false);
                    // TODO: handle custom error to ask for create a new account
                }
            }).catch(err => {
                
                return done(err, false);
            });
    });

/**
 * If added to a express route, it will make the route require the auth token
 */
export function onlyAuthorized() {
    return passport.authenticate('jwt', { session: false });
}

/**
 * Setup the Passport JWT for the given express App.
 * It will add the auth routes (auth, login, logout) to handle the token authorization
 * It will use the mongoDB UserModel to check for user and password
 * Set addDebugRoutes to true for adding a Auth Form for testing purpose
 */
export function setupPassportAuth(app: Application, addDebugRoutes = false) {

    passport.use(jwtStategy);
    // console.log(jwtStategy)
    app.use(passport.initialize());
    // console.log("passport has now initialized we go and compare tokens")
    if (addDebugRoutes) {

        app.get('/auth', (req, res) => {
            const loginFormHtml = `
            <form action="/login" method="post">
                <div>
                    <label>Username:</label>
                    <input type="text" name="username"/>
                </div>
                <div>
                    <label>Password:</label>
                    <input type="password" name="password"/>
                </div>
                <div>
                    <input type="submit" value="Log In"/>
                </div>
            </form>
        `;
            res.send(loginFormHtml);
        });

    }

    app.post('/login', async (req, res, next) => {
        try {
            const { email, password } = req.body;
            
            if (!email || !password) {
                throw new Error('email or password not set in the request');
            }

            // Get user by email
            const user = await  User.findOne({where:{email}});
            // console.log(user)

            if (!user) {
                throw new Error(`User for ${email} could not be found`);
            }

            // Check user password using custom method in jwt 
            const valid = await compare(password,user.password)
            if (!valid)
            {
                throw new Error("Your password failed to match")
            }
            // console.log(valid)
       
            const jwtPayload: JwtPayload = {
                userId: user.id.toString(),
                tokenVersion: user.tokenVersion
            };

            // res.send({message:"Login successful!"})
            // Return a sign token containing the user ID (JwtPayload)
            const token = jwt.sign(jwtPayload, jwtOptions.secretOrKey);            
            console.log("We return the access token then even save it in the localstorage "+token)
                //now token is the accessToken for auth
            res.json({token})

            // if(user.tokenVersion !== jwtPayload.tokenVersion)
            // {
            //     return res.send({ok: false, accessToken:""})
            // }
            //this one here is the refresh token just knoe it 
            res.json(sendRefreshToken(res,createRefreshToken(user))) 

        } catch (error) {
            res.json({
                error: error.message                
            }); 
            
        }
    });

    //i personally added this to it for the refresh token function
    app.post("/refresh_token", async (req , res)=>{
        console.log(req.headers.authorization);
        const token = req.headers.authorization;
        if(!token){
            return res.send({ok: false, accessToken:""})
        }

        let payload : any = null;
        try {
            payload = verify(token, process.env.REFRESH_TOKEN_SECRET) 
            // payload ="";
        }catch(err){
            console.log(err)
            return res.send({ok: false, accessToken:""})            
        }
        //when we reach here we know that the token is valid and we can send back an access token got it!
        const user = await User.findOne({id:payload.userId})//

        if(!user){
            return res.send({ok: false, accessToken:""}) 
        }
        sendRefreshToken(res,createRefreshToken(user) )
        return res.send({ok: true , accessToken: createAccessToken(user)}) 
    }); 


    app.get('/logout', (req, res) => {
        req.logout();
        res.redirect('/');
    });
}
